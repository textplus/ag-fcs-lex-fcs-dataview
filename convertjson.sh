#!/bin/bash

xmlschema-xml2json --schema DataView-Lex.xsd --version 1.1 --converter columnar --force --indent 2 -o examples/ -- examples/*.lex.xml

sed -i 's/"Entry": {/"lexEntries": [{"pid": "", "reference": null,/g' examples/*.lex.json
sed -i 's/^  }/  }]/g' examples/*.lex.json

sed -i 's/"Entry{http:\/\/www.w3.org\/XML\/1998\/namespace}/"/g' examples/*.lex.json

sed -i 's/"Field": /"fields": /g' examples/*.lex.json
sed -i 's/"Fieldtype":/"type":/g' examples/*.lex.json
sed -i 's/"Value": \[/"values": \[/g' examples/*.lex.json

sed -i 's/"Value": "/"value": "/g' examples/*.lex.json
sed -i 's/"Value{http:\/\/www.w3.org\/XML\/1998\/namespace}/"/g' examples/*.lex.json

sed -i 's/"Value": null/"value": null/g' examples/*.lex.json

sed -i 's/"Value/"/g' examples/*.lex.json
