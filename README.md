# LexFCS Data View Schema and Examples

Schema-Document (XSD 1.1): [`DataView-Lex.xsd`](DataView-Lex.xsd)  
Specification (WIP): [`spec/`](spec/)  
Examples: [`examples/`](examples/)

## Installation

To validate XML files or convert them to `JSON` using *convertjson.sh*, install required XSD 1.1 validator as follows:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Validate Examples

```bash
xmlschema-validate --version 1.1 --schema DataView-Lex.xsd examples/*.lex.xml

# to see details add the `-v` flag
xmlschema-validate --version 1.1 --schema DataView-Lex.xsd examples/*.lex.xml -v
```

## Convert Examples to JSON

Convert XML examples to JSON resembling output from FCS Aggregator:

```bash
./convertjson.sh
```

And to combine all the JSON together:

```bash
jq -s '{ lexEntries: [ .[].lexEntries[] ] }' examples/*.lex.json > lexEntries.json
# or assign lemma as pid (WIP)
jq -s '{ lexEntries: [ .[].lexEntries[] | .pid = (.fields[] | select(.type == "lemma") | .values[0].value) ] }' examples/*.lex.json > lexEntries.json
# or assign filename as pid
jq '{ lexEntries: [ .lexEntries[] | .pid = input_filename[9:-9] ] }' -- examples/*.lex.json | jq -s '{ lexEntries: [ .[].lexEntries[] ] }' > lexEntries.json
```
