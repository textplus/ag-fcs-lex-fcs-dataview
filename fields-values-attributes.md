
## Terminology Typography / Conventions

* The key words `MUST`, `MUST NOT`, `REQUIRED`, `SHALL`, `SHALL NOT`, `SHOULD`, `SHOULD NOT`, `RECOMMENDED`, `MAY`, and `OPTIONAL` in this document are to be interpreted as in [RFC2119](https://www.ietf.org/rfc/rfc2119.html).

* `@abc` refers to XML attribute "abc"
* `<abc>` refers to XML element "abc"
* `<abc @def>` refers to XML attribute "def" in XML element "abc"
* Element and attribute names are **case-sensitive**!

- {{{TODO}}} - work in progress, needs contributions

## General Notes

* order of \<lex:Field> `SHOULD NOT` matter, FCS Clients `MAY` rearrange for presentation purposes (e.g., grouping by \<lex:Field @type>)
* order or \<lex:Value> in \<lex:Field> `SHOULD NOT` matter, FCS Clients `MAY` reorder values based on attribute values (e.g., @xml:lang, @preferred) or alphabethically

## Field Types

- first-level:
    - \<lex:Field @type> attribute value
    - in `[`, `]` is type of expected content
- second level
    - explanation of \<lex:Field @type> usage
    - list of attributes for \<lex:Value>
- third level
    - \<lex:Value> attribute usage explanation
    - expected values of attributes

---

* entryId _[text,_ URI]
    * only valid attributes: xml:id, idrefs, type?
* lemma _[text]_
    * xml:lang | langUri
        * indication of language of text
    * type
        * indicate transcription, translation, ...
* phonetic _[text]_ | transcription _[text]_ {{{TODO}}}
* translation _[text]_ {{{TODO}}}
* definition _[text]_ | etymology _[text]_ {{{TODO}}}
* case _[text]_ | number _[text]_ | gender _[text]_
    * vocabRef | vocabValueRef
        * `SHOULD` be used to indicate which vocabulary provided values belong to, e.g., for i18n translations etc.
    * xml:lang
        * `MAY` be useful to indicate what language the provided values is in
* pos _[text]_
    * vocabRef | vocabValueRef
        * `MUST` be provided unless _unknown_
* segmentation _[text]_
    * \<lex:Value> `SHOULD` use the `|` character as separator to allow for post-processing on FCS Client side (e.g., exchange of separators, splitting of parts), other separator characters (`+`, `-`, `·`, ...) are permissible
    * type
        * `SHOULD` indicate what type of segmentation
        * expected values: hyphenation, worttrennung, ...? {{{TODO}}}
* sentiment _[text]_
    * should value be restricted to some [-1,+1] scale or are categories allowed
    * vocabRef | vocabValueRef
        * `RECOMMENDED` to explain value
* antonym _[text]_ | hyponym _[text]_ | hypernym _[text]_ | meronym _[text]_ | holonym _[text]_ | synonym _[text]_ | related _[text]_ {{{TODO}}}
* ref _[URI]_
    * type
        * indication of type of `ref`
* senseRef _[text]_
    - reference to external sense definitions
    - value is textual identifier/label for sense
    - usage `MAY` be identifier in Dornseiff, GermaNet, Word Net, WikiData, ...
    * vocabRef | vocabValueRef
        * URI where identifier is defined
    * idrefs
        * reference to `lemma`, ... with this sense 
* citation _[text]_
    - citation/source/provenance information for other \<lex:Value>
    * idrefs
        * reference(s) to other \<lex:Value> this `citation` is for
    * type
        * what type of citation? what is being provided by this citation?
        * values: examples, ...
    * source
        * human readable label for source
    * sourceRef
        * URI to source
    * date
        * date of access/publication/?
    * rel
        * convenience attribute, referencing \<lex:Field> types; @idrefs reference \<lex:Field> (\<lex:Value>) should provide final truth (when conflicting @rel and \<lex:Field @type>)

## Attributes

* xml:id _[XML identifier]_
    * unique id per \<lex:Entry>
* xml:lang _[ISO639, BCP47, ...]_
    * language of \<lex:Value> (text content)
    * inherits default from \<lex:Entry @xml:lang>
* langUri _[URI]_
    * provide additional information using URI for @xml:lang attribute, requires @xml:lang attribute to be set
    * inherits default from \<lex:Entry @langUri>
* preferred _[boolean]_
    * `SHOULD` be used for at least one \<lex:Value> in each \<lex:Field> to indicate (a) which \<lex:Value> does represent the \<lex:Field @type> best, (b) is the best value for a set of options, (c) ...
    * Value: "`true`"
* ref _[URI]_
    * URI with more information
* idRefs _[space separated list of @xml:id]_
    * references another \<lex:Value> and indicates some kind of relation, primarily used for grouping and connecting `definition`/`cit` with their values
* vocabRef _[URI]_
    * reference to vocabulary that contains one or multiple values and additional information
* vocabValueRef _[URI]_
    * like `vocabRef` but only points to additional information of a single vocabulary value
* type _[text]_
    * indicates different variants of \<lex:Value> values
* source _[text]_ | sourceRef _[URI]_ | date _[date]_ | rel _[one of \<lex:Field @type>]_
    * only for `cit`
