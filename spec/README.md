# LexFCS Specification Draft

## Requirements

Generating PDF output requires the fallback-font "VLGothic" to display certain unicode characters correctly. For more details, see [Github Asciidoctor-PDF Issue 1129](https://github.com/asciidoctor/asciidoctor-pdf/issues/1129) and the [Asciidoctor-PDF docs](https://docs.asciidoctor.org/pdf-converter/latest/theme/font-support/).

```apt
# debian
apt install fonts-vlgothic
```

## How to build

```bash
# Output will be placed in `docs/`
# Cleanup
rm -r docs/ 2>/dev/null

# Build HTML
asciidoctor -v -D docs -a data-uri --backend=html5 -o lexfcs.html index.adoc

# Build PDF
asciidoctor-pdf -v -D docs -o lexfcs.pdf index.adoc

# (optional) Copy attachments
cp -R attachments docs/

# (optional) Copy examples (are already included into the documents)
cp -R examples docs/
```

You can use the [`asciidoctor/docker-asciidoctor` docker image](https://github.com/asciidoctor/docker-asciidoctor/blob/main/README.adoc) for building the specifications if you don't want to install various dependencies. Note that the build artifacts belong to the `root` user.

```bash
docker run --rm -it -v $(pwd):/documents asciidoctor/docker-asciidoctor
# optional with `-v /usr/share/fonts/truetype:/usr/share/fonts/truetype:ro` to mount local fonts into container

# then run your build commands
# ...
# fix permissions to belong to user/group with id 1000 (or provide a name)
# chown -R 1000:1000 docs/
```
