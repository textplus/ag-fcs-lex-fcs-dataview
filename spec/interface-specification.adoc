= Summary and Interface Specification
:description: SRU Explain and SearchRetrieve for FCS 2.0

== Summary of Changes

This specification extends the <<ref:CLARIN-FCSCore20,_CLARIN Federated Content Search (CLARIN-FCS) - Core 2.0_>> specification in the following ways:

* introducing a new query language, <<LexCQL>>, based on <<ref:LOC-CQL,Contextual Query Language (CQL)>> to allow querying lexical resources,
* extending the basic <<Extension of the Hits Data View for LexFCS,Hits Data View>> for inline markup, and adding a required <<Lexical Data View>> for searching through lexical resources,
* extending the CLARIN-FCS _Endpoint Description_ with the _Lexical Search_ Capability.


== Discovery

The CLARIN-FCS SRU _explain_ response is extended by adding the _Lexical Search_ capability and _Lexical_ Data View to the _Endpoint Description_ to support client auto-configuration.


=== Capabilities

The _Lexical Search_ capability indicates to clients that the FCS endpoint supports searches through lexical resources using <<LexCQL>> and serializes results in the <<LexFCS Data Views,Data Views>>.

.New Lexical Search Capability
[%header,cols="1e,3m,2"]
|===
|Name
|Capability Identifier
|Summary

|Lexical Search
|\http://clarin.eu/fcs/capability/lex-search
|Structured search for lexical resources
|===


=== Endpoint Description

The _Endpoint Description_ remains unchanged. The `<ed:SupportedDataView>` element supports the additional <<Lexical Data View>> which has its own MIME type and `@id` value.


== Searching and Result presentation

Queries `MUST` be formulated using the <<LexCQL>> query language. Results `MUST` be serialized using both the _Generic Hits_ Data View as defined in the CLARIN FCS Core Specification and the <<Lexical Data View>>. For the _Generic Hits_ Data View, the <<Extension of the Hits Data View for LexFCS>> `MAY` be used, which supports limited inline markup to provide extra context to Hits.
// TODO: Need explanation for why limited markup relevant: "which supports limited inline markup that allows users to X"

// Clients may decide what they support for rendering when using the LexHits Data View.